
/*[ARROWS]	38 - up, 37 - left, 40 - down, 39 - right
[WASD]	87 - up, 65 - left, 83 - down, 68 - right
[PAUSE]	32 - space */

INPUT_UP 	= [38,87];
INPUT_RIGHT = [39,68];
INPUT_DOWN  = [40,83];
INPUT_LEFT  = [37,65];
INPUT_PAUSE = [32];

var keyPressQue = [];

Array.prototype.pushUnique = function (item){
	if(!this.contains(item) ) 
		this.push(item);
}
Array.prototype.contains = function (item){
	return (this.indexOf(item) > -1) ;
}

function keyEventListener(event) {
	var keyPressed = event.keyCode;
	if ( INPUT_UP.contains(keyPressed) ){
		keyPressQue.pushUnique("INPUT_UP");		
	}
	if ( INPUT_RIGHT.contains(keyPressed) ){
		keyPressQue.pushUnique("INPUT_RIGHT");	
	}
	if ( INPUT_DOWN.contains(keyPressed) ){
		keyPressQue.pushUnique("INPUT_DOWN");
	}
	if ( INPUT_LEFT.contains(keyPressed) ){
		keyPressQue.pushUnique("INPUT_LEFT");
	}
	if ( INPUT_PAUSE.contains(keyPressed) ){
		keyPressQue.pushUnique("INPUT_PAUSE");
	}		
}

function pollKeyEvents(){
	var t = keyPressQue.slice();
	keyPressQue = [];
	return t;
}
// var keyVectors = {
// 		"INPUT_UP": 	[ 0, 1],
// 		"INPUT_RIGHT": 	[ 1, 0],
// 		"INPUT_DOWN": 	[ 0,-1],
// 		"INPUT_LEFT": 	[-1, 0]
// };
