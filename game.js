//GAME VARIABLES
var canvas;
var ctx;
var gameTimeout;
var running = true;

//playfield
var pixelRatio = 32;
var gameWidth = pixelRatio * 32;
var gameHeight = pixelRatio * 32;
var fps = 15;

//snake
var snake;

//key presses
var moveQuery = [];

// collision items
var collisionMap = [];

window.onload = function() {
	gameInit();
};

function gameInit() {
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");
	x = gameWidth / 2 / 32;
	y = gameWidth / 2 / 32;
	
	snake = new Snake([x,y], [x,y-1]);
	
	draw();
	moveQuery.push([1, 0]);
	
	lastPressed = 39;
	keysAvailable = true;
	
	gameLoop();
}

function gameLoop() {
	var gameTimeout = setTimeout(function() {
		if (running) {
			inputHandler();
			requestAnimationFrame(gameLoop);
			update();
			draw();
		}
	}, 1000 / fps);
}



//updating coordinates for drawing
function update() {
	if (moveQuery.length)
		snake.move(  moveQuery.shift() );
	else
		snake.move();	
	hasCollided();	
}

//draws game
function draw() {
	ctx.clearRect(0, 0, gameWidth, gameHeight);
	//drawGrid();
	generateWalls();
	drawSnake(); 
}

//gamefield drawing logcis
function drawGrid() {
	ctx.clearRect(0, 0, gameWidth, gameHeight); // wrong place ???
	ctx.lineWidth = "1";
	ctx.strokeStyle = "#616161";
	for (i = 0; i <= gameWidth; i += pixelRatio) {
		for (j = 0; j <= gameHeight; j += pixelRatio) {
			ctx.strokeRect(i, j, pixelRatio, pixelRatio);
		}
	} 
}

//snake drawing logics
function drawSnake() {
	ctx.beginPath();
	ctx.fillStyle= "green";
	var drawList = snake.drawShapes();
	for (var i = 0; i < drawList.length; i++) {
		var args = drawList[i];
		ctx.fillRect(args[0] * pixelRatio, args[1] * pixelRatio,
		args[2] * pixelRatio, args[3] * pixelRatio);	
	};
	ctx.fillStyle= "black";
}


function inputHandler() {
	var keyEvents = pollKeyEvents();
	var keyVectors = {
		"INPUT_UP": 		[ 0,-1],
		"INPUT_RIGHT": 	[ 1, 0],
		"INPUT_DOWN": 	[ 0, 1],
		"INPUT_LEFT": 		[-1, 0]
	}
	for (var i = keyEvents.length - 1; i >= 0; i--) {
		var keyEvent = keyEvents[i];
		if (keyEvent=="INPUT_PAUSE"){
			// pause();
			snake.eat();
			console.log("ATEE");
		}
		else {
			var movementVector = keyVectors[keyEvent];
			if (movementVector) moveQuery.push(movementVector);
		}
	};
}


//pauses the game loop
function pause() {
	running = !running
	if (!running)
		clearTimeout(gameTimeout);
	else 
		gameLoop();
}

function generateWalls() {
	if (!Object.keys(collisionMap).length) {
		for (x = 0; x <= gameWidth / pixelRatio; x += gameWidth / pixelRatio) {
			for (y = 0; y <= gameHeight / pixelRatio; y += 1) {
				var block = [x, y];
				collisionMap[block] = block;
			}
		} 
		for (y = 0; y <= gameWidth / pixelRatio; y += gameWidth / pixelRatio) {
			for (x = 0; x <= gameWidth/pixelRatio; x += 1) {
				var block = [x, y];
				collisionMap[block] = block;		
			}		
		} 	
	}
	for (var key in collisionMap) {
		var block = collisionMap[key];		
		//ctx.fillRect(block[0] * pixelRatio - pixelRatio / 2, block[1] * pixelRatio - pixelRatio / 2, 
		//pixelRatio, pixelRatio);	
	};
}

function hasCollided () {
	var collisions = snake.getCollisions(collisionMap);
	if (collisions.length) pause();

	// collisions with itself
	collisions = snake.getSelfCollisions();
	if (collisions.length) pause();
}