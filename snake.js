
function addV(a,b){
	return [a[0]+b[0],a[1]+b[1]];
}

function subV(a,b){
	return [a[0]-b[0],a[1]-b[1]];
}

function flip(a){
	return [a[1],a[0]]
}

Array.prototype.contains = function (item){
	return (this.indexOf(item) > -1) ;
}

function horisontalRect(a,b,s){
	var upperR = a;
	var lowerL = b;	
	if (a[0]>b[0]){
		upperR = b;
		lowerL = a;			
	}
	var delta = subV(lowerL,upperR);
	var C=lowerL[0]-upperR[0];
	return [upperR[0]-s,upperR[1]-s,C+s*2,2*s];
}

function rectV(a,b,s){
	a = a.slice(); b = b.slice();
	var horisontal = a[0]!=b[0];
	var vertical =  a[1]!=b[1];

	if (horisontal){
		return horisontalRect(a,b,s);
	} 
	if (vertical){
		a = flip(a);
		b = flip(b);
		var temp = horisontalRect(a,b,s);
		temp = [  temp[1],temp[0],temp[3],temp[2]  ]
		return temp;
	}
}
 
function Snake (start,butt) {
    
    this.color 		= "red";
    this.thickness  	= 0.2;

    this.body 		= [start,butt]; // body part cords
    this.digestion 	= []; // food coords, when pooped -> add to body

    this.lastDirection	= [0,0];

    this.eat = function() {
        this.digestion.push(this.body[0])
    };

    this.grow = function(butt) {
    	for (var i = this.digestion.length - 1; i >= 0; i--) {
        	var food = this.digestion[i];
        	if (food === butt){ // gotta love programming
        		this.body.push(food);
        		this.digestion.splice(i, 1);
        		i = this.digestion.length - 1;
        		return;
        	}
        };
    };
    
   	this.move = function(delta) {
   		if (!delta){
   			delta = this.lastDirection;
   		}
    	var lastPos = addV(delta,this.body[0]);
    	var change = addV(delta,this.lastDirection);
    	if (!change[0] && !change[1]) return; // retard tried to go backwards
    	
    	for (var i = 0; i < this.body.length; i++) {
    		var currentPos = this.body[i];
    		this.body[i] = lastPos;
    		lastPos = currentPos;        	
        };
        this.grow(lastPos);
        this.lastDirection = delta;
    };

    this.drawShapes = function(delta) {
    	var drawList = [];
    	for (var i = 0; i < this.body.length -1; i++) {
    		var currentPos = this.body[i];
    		var nextPos = this.body[i+1];
    		drawList.push( rectV(currentPos,nextPos,this.thickness) );    		
        };  
        for (var i = 0; i < this.digestion.length ; i++) {
            var currentPos = this.digestion[i];	
            var d =  this.thickness*2;	
            var A = currentPos[0]-d, B = currentPos[1]-d;
    		drawList.push( [A,B,d*2,d*2 ] );     		
        };       
       return drawList;
    }
    
    this.getSelfCollisions = function() {
        var copyBody = this.body.slice();
        var checkDict = [];
        var collisions = [];
        while (copyBody.length) {
            var bodyPart = copyBody.shift();
            if (checkDict.hasOwnProperty( bodyPart ) )
                collisions.push(bodyPart);
            checkDict[bodyPart] = bodyPart;

        }  
        return collisions;
    }
    
    this.getCollisions = function(collisionMap){
        var collisions = [];

        for (var i =  this.body.length - 1; i >= 0; i--) {
            var bodyPart = this.body[i];
            // console.log(bodyPart);
            if (collisionMap.hasOwnProperty( bodyPart )){
                collisions.push(bodyPart);
            }
        };     
        return collisions;
    }
}

// unit tests

var snake = new Snake([0,0],[0,-1]);

console.log("start body",snake.body[0],snake.body[1]);

snake.move([0,1]);
snake.move([0,1]);
snake.move([1,0]);
snake.move([0,1]);

console.log("move body",snake.body[0],snake.body[1]);

snake.eat()

console.log("digest",snake.digestion[0]);

snake.move([0,1]);
snake.move([0,1]);


console.log("move and grow body",snake.body[0],snake.body[1],snake.body[2]);


var A = [2,2];
var B = [6,2];

console.log( "rect H 1 ", rectV(A,B,0.5) );
console.log( "rect -H 2 ", rectV(B,A,0.5) );

 A = [2,2];
 B = [2,8];

console.log( "rect V 1 ", rectV(A,B,0.5) );
console.log( "rect -V 2 ", rectV(B,A,0.5) );


console.log( "snake draw boxes ");
snake.move([1,0]);
var shapes = snake.drawShapes();

for (var i = 0; i < shapes.length; i++) {
	console.log( shapes[i] );
};